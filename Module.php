<?php

namespace skmnt\skyadmin;

/**
 * admin module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'admin\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        \Yii::setAlias('@admin', __DIR__);
        \Yii::$app->errorHandler->errorAction = '/admin/default/error';
        \Yii::$app->set('personal', new \admin\components\PersonalService);

        $this->layout = 'main';
        $this->layoutPath = __DIR__ . '/views/layouts/';

        \Yii::setAlias('@uploadDir', $uploadDir = \Yii::getAlias('@runtime') . '/temp_upload');
        if (!file_exists($uploadDir)) {
            @mkdir($uploadDir, 0777, true);
        } 
    }
}
