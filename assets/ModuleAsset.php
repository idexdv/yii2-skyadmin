<?php

namespace admin\assets;

use yii\web\AssetBundle;

class ModuleAsset extends AssetBundle
{
	public $sourcePath = __DIR__ . '/css';
	public $css = [
		'admin.css',
	];
	public $js = [
	];
	public $depends = [
		'yii\web\YiiAsset',
        'plugins\assets\LightBoxAsset',
        'plugins\assets\JsCookieAsset',
 	];
 	public $publishOptions = [
 		'forceCopy' => true,
 	];
}