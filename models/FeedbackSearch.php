<?php

namespace admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Feedback;

class FeedbackSearch extends Feedback
{
    public $search;

    function __construct($type)
    {
        $this->type = $type;
    }

    public function rules()
    {
        return [
            ['search', 'trim'],
            ['search', 'string'],
            [['customer_name', 'customer_phone', 'customer_email'], 'string'],
        ];
    }

    public function behaviors() {
        return [];
    }

    public function beforeValidate()
    {
        return \yii\base\Model::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Feedback::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => [
                'created_at' => SORT_DESC,
            ]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if (!empty($this->search)) {
            $query
                ->andWhere(['or',
                    ['like', 'customer_name', $this->search],
                    ['like', 'customer_email', $this->search],
                    ['like', 'customer_phone', $this->search],
                ]);
        } else {
            $query
                ->andFilterWhere(['like', 'customer_name', $this->customer_name])
                ->andFilterWhere(['like', 'customer_email', $this->customer_email])
                ->andFilterWhere(['like', 'customer_phone', $this->customer_phone]);
        }

        return $dataProvider;
    }
}
