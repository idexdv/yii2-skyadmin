<?php

namespace admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Page;

class PageSearch extends Page
{
    public $search;

    function __construct($type)
    {
        $this->type = $type;
    }

    public function rules()
    {
        return [
            ['search', 'trim'],
            ['search', 'string'],
            ['parent_id', 'integer'],
            [['title', 'slug', 'static_url'], 'string'],
        ];
    }

    public function behaviors() {
        return [];
    }

    public function beforeValidate()
    {
        return \yii\base\Model::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function getParent()
    {
        return $this->parent_id ? Page::findOne($this->parent_id) : null;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Page::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => [
                'ordering' => SORT_ASC,
                'created_at' => SORT_DESC,
            ]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if (!empty($this->search)) {
            $query
                ->andWhere(['type' => $this->type])
                ->andWhere(['or',
                    ['like', 'slug', $this->search],
                    ['like', 'title', $this->search],
                    ['like', 'static_url', $this->search],
                ]);
        } else {
            $query
                ->andWhere(['type' => $this->type])
                ->andWhere(['parent_id' => $this->parent_id])
                ->andFilterWhere(['like', 'slug', $this->slug])
                ->andFilterWhere(['like', 'title', $this->title])
                ->andFilterWhere(['like', 'static_url', $this->static_url]);
        }

        return $dataProvider;
    }
}

