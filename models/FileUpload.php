<?php

namespace admin\models;

use yii\base\Model;
use yii\web\UploadedFile;

class FileUpload extends Model
{
    /**
     * @var UploadedFile
     */
    public $imageFile;
    public $oid;
    public $mcl;

    public $filename;
    public $filepath;

    /**
     * CKEditor Upload Feature
     */
    public $upload;

    public function rules()
    {
        return [
            [['imageFile', 'upload'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg'],
            ['oid', 'integer'],
            ['mcl', 'string', 'max' => 64],
        ];
    }
    
    public function upload($skipValidation = false)
    {
        if ($this->validate() || $skipValidation) {
            $this->filename = hash('md5', $this->imageFile->baseName . microtime() . rand(0, 999)) . ".{$this->imageFile->extension}";
            $this->imageFile->saveAs($this->filepath = \Yii::getAlias('@uploadDir') . '/' . $this->filename);
            return true;
        } else {
            return false;
        }
    }

    public function cleanUp()
    {
        @unlink($this->filepath);
    }

    public function getModel()
    {
        $class = '\common\models\\' . $this->mcl;
        if (class_exists($class)) {
            return $class::findOne($this->oid);
        }
    }
}