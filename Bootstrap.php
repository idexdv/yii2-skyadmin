<?php
namespace skmnt\skyadmin;

use yii\base\BootstrapInterface;
use yii\base\Application;

class Bootstrap implements BootstrapInterface
{
    public function bootstrap($app)
    {
        if (get_class($app) == 'yii\web\Application') {
            $app->on(Application::EVENT_BEFORE_REQUEST, function() {
                \Yii::$app->urlManager->addRules([
                    'admin/page/index/<type:[a-z-]+>' => 'admin/page/index',
                    'admin/page/create/<type>' => 'admin/page/create',
                    'admin/navigator/index/<type:[a-z]+>' => 'admin/navigator/index',
                    'admin/navigator/create/<type>' => 'admin/navigator/create',
                ]);
            });
        }
    }
}

