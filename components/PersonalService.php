<?php
namespace admin\components;

use common\models\User;

class PersonalService extends \yii\base\Component
{
	public $name;
	private $user;

	function __construct($argument = [])
	{
		if (\Yii::$app->user->isGuest) {
		} else {
			$this->user = \Yii::$app->user->identity;
			$this->name = $this->user->username;
		}
	}

	public function authGE($roleLevel)
	{
		return $this->user->role >= $roleLevel;
	}

	public function can($roles)
	{
		$items = explode(',', $roles);
		array_walk($items, 'trim');
		foreach ($items as $item) {
			$constName = "USER_{$item}"; 
			if ($this->user->role >= constant(User::class . "::{$constName}")) return true;
		}
		return false;
	}

    public function isMe($id)
    {
    	return $this->user->id == $id;
    }
}
