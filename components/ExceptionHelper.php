<?php

namespace admin\components;

/**
* ExceptionToAlert class
*/
class ExceptionHelper
{
	private $exception;
	private $contextAction;

	function __construct(\Exception $e, $ca)
	{
		$this->exception = $e;
		$this->contextAction = $ca;
	}

	public function toAlert()
	{
        \Yii::$app->session->setFlash('danger', sprintf('Ошибка во время: %s', $this->contextAction));
        switch ($this->exception->getCode()) {
        	case 23000:
        		\Yii::$app->session->addFlash('warning', 'От этой записи зависят другие объекты, удалите сперва их.');
        		break;
        }
	}
}
