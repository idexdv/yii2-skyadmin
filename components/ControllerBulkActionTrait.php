<?php
namespace admin\components;

use Yii;

/**
* Controller bulk action trait
*/
trait ControllerBulkActionTrait
{
    public function actionBulkAction()
    {
        if (!class_exists($this->baseClass)) {
            throw new \yii\base\InvalidConfigException('Ошибка обработки, не задан целевой класс.');
        }

        $data = \yii\base\DynamicModel::validateData(Yii::$app->request->post(), [
            ['action', 'string', 'max' => 64],
            ['keys', 'each', 'rule' => ['integer', 'min' => 0]],
        ]);

        if (!$data->hasErrors()) {
            $_baseClass = $this->baseClass;
            foreach ($data['keys'] as $id) {
                if ($model = $_baseClass::findOne($id)) {
                    switch ($data['action']) {
                        case 'delete-it':
                            $model->delete();
                            break;
                        case 'active-on':
                            $model->active = 1;
                            $model->save();
                            break;
                        case 'active-off':
                            $model->active = 0;
                            $model->save();
                            break;
                    }
                } else {
                    return $this->asJson(['result' => false]);            
                }
            }
            return $this->asJson(['result' => true]);
        }
        return $this->asJson(['result' => false]);
    }

    public function actionSortAction()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        parse_str(\Yii::$app->request->post('data'), $_itemsOrder);
        if(!is_array($_itemsOrder) || empty($_itemsOrder)) return ['result' => false];
        $itemsOrder = array_values($_itemsOrder);
        $className = $this->baseClass;
        foreach ($itemsOrder as $key => $id) {
            if ($model = $className::findOne($id)) {
                $model->ordering = 1 + $key;
                $model->save();
            }
        }
        return ['result' => true];
    }
}
