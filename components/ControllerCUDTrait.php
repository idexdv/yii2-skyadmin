<?php

namespace admin\components;

trait ControllerCUDTrait
{
    public function actionIndex($type, $parent_id = null)
    {
        $this->type = $this->type2id($type);
        return $this->renderIndex($parent_id);
    }

    private function renderIndex($parent_id)
    {
        $searchModel = new $this->searchClass($this->type);
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams + [$searchModel->formName() => ['parent_id' => $parent_id]]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'cid' => $this->cid,
            'title' => $this->title,
            'parent' => $searchModel->isDepth ? $searchModel->parent : null,
        ]);
    }

    public function getCid()
    {
        $_class = '\\' . $this->baseClass;
        return @$_class::$controllerIds[$this->type];
    }

    public function getTitle()
    {
        $_class = '\\' . $this->baseClass;
        return $_class::$controllerTitles[$this->type];
    }

    public function type2id($typeCode)
    {
        $_class = '\\' . $this->baseClass;
        $id = array_search($typeCode, $_class::$controllerIds);
        if ($id === false) {
            throw new \yii\web\BadRequestHttpException('Wrong controller\'s type');
        }
        return $id;
    }

    public function actionCreate($type, $parent_id = null)
    {
        // $faker = \Faker\Factory::create();
        $this->type = $this->type2id($type);
        $model = new $this->baseClass([
            'type' => $this->type,
            // 'template' => 0,
            // 'content' => $faker->text(500, 3),
        ]);

        if ($parent_id !== null && in_array('parent_id', $model->attributes())) {
            $model->parent_id = $parent_id;
        }

        if (method_exists($model, 'propSave')) $model->propSave();

        if ($model->load(\Yii::$app->request->post()) && $model->validate() && $model->preProcess() && $model->save()) {
            if (!isset($_POST['exitmode']))
                return $this->redirect(["{$this->rootController}/update", 'id' => $model->id]);    
            else {
                return $this->redirect(["{$this->rootController}/index", 'type' => $this->cid,
                        (new $this->searchClass($model->type))->formName() => ['parent_id' => $model->parent_id]]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
                'indexTitle' => $this->title,
                'indexAction' => ["{$this->rootController}/index", 'type' => $this->cid],
                'indexSearchId' => (new $this->searchClass($model->type))->formName()
            ]);
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (method_exists($model, 'propSave')) $model->propSave();

        if ($model->load(\Yii::$app->request->post()) && $model->preProcess() && $model->save()) {
            if (!isset($_POST['exitmode']))
                return $this->redirect(["{$this->rootController}/update", 'id' => $model->id]);
            else {
                return $this->redirect(["{$this->rootController}/index", 'type' => $this->cid,
                        (new $this->searchClass($model->type))->formName() => ['parent_id' => $model->parent_id]]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
                'indexTitle' => $this->title,
                'indexAction' => ["{$this->rootController}/index", 'type' => $this->cid],
                'createAction' => ["{$this->rootController}/create", 'type' => $this->cid, 'parent_id' => $model['parent_id']],
                'indexSearchId' => (new $this->searchClass($model->type))->formName(),
            ]);
        }
    }

    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if (property_exists($model, 'parent_id')) {
            foreach ($model::find()->where(['parent_id' => $id])->all() as $page) {
                $this->actionDelete($page->id);
            }
        }

        try {
            $model->delete();
        } catch (\Exception $e) {
            (new \admin\components\ExceptionHelper($e, 'Удаление записи'))->toAlert();
        }

        return $this->redirect(["{$this->rootController}/index", 'type' => $this->cid]);
    }

    public function actionActive($id)
    {
        $model = $this->findModel($id);
        $model->active = $model['active'] != 1 ? 1 : 0;
        $model->save();
        return $this->redirect(\Yii::$app->request->referrer ?: ["{$this->rootController}/index", 'type' => $this->cid]);
    }

    protected function findModel($id)
    {
        $_class = $this->baseClass;
        if (($model = $_class::findOne($id)) !== null) {
            if (isset($model['type'])) {
                $this->type = $model['type'];
            }
            return $model;
        } else {
            throw new \yii\web\NotFoundHttpException('Требуемая страница не найдена.');
        }
    }
}

