<?php

namespace admin\components;

class ConsoleHelper extends \yii\base\Component
{
    public function init()
    {
        parent::init();
        set_time_limit(0);
    }

    public function runAction($action, &$result = '')
    {
        error_reporting(E_ALL);
        $command = $this->buildCommand($action);
        $process = popen($command, 'r');
        while(!feof($process)) $result .= fgets($process);
        return pclose($process);
    }

    protected function buildCommand($action)
    {
        $yiiEntryPoint = dirname(\Yii::getAlias('@app'));
        return $this->isWindows()
                ? sprintf('start /b php %s/yii %s', $yiiEntryPoint, $action)
                : sprintf('%s/php %s/yii %s > /dev/null 2>&1 &', PHP_BINDIR, $yiiEntryPoint, $action);
    }

    protected function isWindows()
    {
        return in_array(PHP_OS, ['WINNT', 'WIN32']);
    }
}
