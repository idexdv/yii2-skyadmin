<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\Inflector;

switch ($type) {

	case 'integer':
?>
	<div class="form-group">
		<label for="input-id"><?= $label ?></label>
		<input type="integer" name="S[<?= $idk ?>]" class="form-control" value="<?= unserialize($data) ?>">
	</div>
<?
		break;

    case 'url':
?>
    <div class="form-group">
        <label for="input-id"><?= $label ?></label>
        <input type="url" name="S[<?= $idk ?>]" class="form-control" value="<?= unserialize($data) ?>">
    </div>
<?
        break;

    case 'textstyled':
?>
    <div class="form-group">
        <label for="<?= $idk ?>"><?= $label ?></label>
        <div class="clearfix"></div>
        <div>
            <textarea id="<?= $idk ?>" data-styled-editor data-editor-height="300" name="S[<?= $idk ?>]" class="form-control"><?= unserialize($data) ?></textarea>
        </div>
    </div>
<?
        break;

    case 'text':
?>
    <div class="form-group">
        <label for="<?= $idk ?>"><?= $label ?></label>
        <div class="clearfix"></div>
        <div>
            <textarea id="<?= $idk ?>" name="S[<?= $idk ?>]" style="min-height: <?= intval(mb_strlen($data)/5) + 70 ?>px;" class="textblock form-control"><?= unserialize($data) ?></textarea>
        </div>
    </div>
<?
        break;

    case 'string':
    default:
?>
    <div class="form-group">
        <label for="input-id"><?= $label ?></label>
        <input type="text" name="S[<?= $idk ?>]" class="form-control" value="<?= unserialize($data) ?>">
    </div>
<?
        break;        		
}
