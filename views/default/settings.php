<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Inflector;

$this->title = 'Настройки';
$settings = \common\models\Setting::find()->orderBy('idk ASC')->indexBy('idk')->asArray()->all();
$list = [];
$free = [];

foreach ($settings as $key => $item) {
    @list($name, $type) = explode('_', ucwords($key));
    $uName = Inflector::camel2words($name);
    $nameParts = explode(' ', $uName);
    if ($type !== null && (count($nameParts) >= 2)) {
        $list[$tab = array_shift($nameParts)][0] = ['tabLabel' => $tab];
        $list[$tab][] = [
            'label' => implode(' ', $nameParts),
            'data' => $item['data'],
            'type' => $type,
            'idk' => $key,
        ];
    } else {
        $free[] = [
            'label' => implode(' ', $nameParts),
            'data' => $item['data'],
            'type' => null,
            'idk' => $key,
        ];
    }
}
if (!empty($free)) {
    $list['Others'] = [0 => ['tabLabel' => 'Others']] + $free;
}

?>

<?php $form = ActiveForm::begin(['id' => 'update-form']); ?>

    <?
        echo \yii\bootstrap\Tabs::widget([
            'items' => array_map(function($el) {
                return [
                    'label' => array_shift($el)['tabLabel'],
                    'content' => sprintf('<div class="well">%s</div>', implode(array_map(function($item){
                        return $this->render('settings-render', $item);
                    }, $el))),
                ];
            }, $list),
            'clientEvents' => [
                'show.bs.tab' => new \yii\web\JsExpression('function(e) { location.hash = e.target.hash; }'),
            ],
        ]);
    ?>

    <div class="form-group"><?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?></div>

<?php ActiveForm::end(); ?>

<? $this->registerJs("var hash = window.location.hash; hash && $('ul.nav a[href=\"' + hash + '\"]').tab('show');"); ?>

<style type="text/css">
    .textblock {
        min-height: 100px;
        max-height: 700px;
        resize: vertical;
    }
</style>
