<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Page;

/* @var $this yii\web\View */
/* @var $model common\models\Page */
/* @var $form yii\widgets\ActiveForm */

$formId = 'update-form';
$targetInputId = Html::getInputId($model, 'target');
if ($target = $model->target) {
    list($type, $targetId) = explode(':', $target);
}
?>

<style>
@media (min-width: 1299px) {
    .with-image-aside {
        padding-right: 340px;
    }
}
</style>

<div class="panel with-image-aside">

    <?php $form = ActiveForm::begin(['id' => $formId]); ?>

        <?= ($_errorHtml = $form->errorSummary($model)) ? '<div class="col-sm-12 text-danger">'.$_errorHtml.'</div>' : '' ?>

        <div class="panel-body">
            <div class="row">

                <? if ($model->isDepth): ?>
                    <?= $form->field($model, 'title', ['options' => ['class' => 'col-md-9']]) ?>
                    <?= $form->field($model, 'parent_id', ['options' => ['class' => 'col-md-3']])->dropDownList($model->parentsList(), ['prompt' => 'Корневой уровень']) ?>
                <? else: ?>
                    <?= $form->field($model, 'title', ['options' => ['class' => 'col-md-12']]) ?>
                <? endif; ?>

                <?= $form->field($model, 'url', ['options' => ['class' => 'col-md-9']]) ?>
                <?= $form->field($model, 'active', ['options' => ['class' => 'col-md-3']])->dropDownList(['1' => 'Активен', '0' => 'Выключен']) ?>

                <?= $form->field($model, 'target', [
                        'options' => ['class' => 'col-md-12 form-group'],
                        'template' => '{label}'.
                            (empty($target) ? '{input}'
                                : '<div class="input-group">
                                    <span class="input-group-btn">'.
                                        Html::a('Перейти к объекту', ['/admin/page/update', 'id' => $targetId], ['class' => 'btn btn-default', 'target' => '_blank']).
                                    '</span>{input}<span class="input-group-btn">'.
                                    Html::button('Сбросить привязку', [
                                        'title' => 'После нажатия на кнопку, форма будет сохранена!',
                                        'class' => 'btn btn-primary',
                                        'type' => 'button',
                                        'onclick' => new \yii\web\jsExpression("if(confirm('Отменить ведение объекта навигатором?')) { $( '#{$targetInputId}' ).val(''); $( '#{$formId}' ).submit(); }")
                                    ]) . '</span></div>'
                            ) . '{hint}{error}'])
                    ->textInput(['placeholder' => 'Если поле пустое - это значит, что навигатор не следит за объектом', 'readonly' => true, 'title' => 'Это служебная информация, которая используется Навигатором для ведения объекта перехода.'])
                    ->label(false) ?>

                <?= \admin\widgets\form\NavSelector::widget(['form' => $form, 'model' => $model, 'targetId' => $targetInputId]) ?>

                <?= \admin\widgets\form\DynamicForm::widget(['form' => $form, 'model' => $model]) ?>

            </div>
        </div>

        <div class="panel-footer col-md-12" style="background-color: #222d32;">
            <?= Html::a('Сохранить и закрыть', null, [
                'onclick' => 'SaveAndExit(this);',
                'class' => 'pull-right btn btn-success',
            ]) ?>
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
        </div>

    <?php ActiveForm::end(); ?>

    <div class="clearfix"></div>

</div>
