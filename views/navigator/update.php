<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Page */

$this->title = 'Изменение: ' . mb_substr($model->title, 0, 60);
$this->params['breadcrumbs'][] = ['label' => $indexTitle, 'url' => $indexAction];
if ($parent = $model->parent) {
    $this->params['breadcrumbs'][] = ['label' => $parent['title'], 'url' => $indexAction + ['parent_id' => $parent['id']]];
}
$this->params['breadcrumbs'][] = 'Изменение';
$this->params['breadcrumbs'][] = ['label' => '(Создать запись)', 'url' => @$createAction, 'encode' => false];
?>
<div class="custom-page-update">

    <?= \yii\bootstrap\Tabs::widget([
        'items' => [
            [
                'label' => 'Общее',
                'content' => $this->render('_form', ['model' => $model]),
                'active' => true,
                'options' => ['id' => 'general'],
            ],
        ],
        'clientEvents' => [
            'show.bs.tab' => new \yii\web\JsExpression('function(e) { location.hash = e.target.hash; }'),
        ],
    ]) ?>

    <? $this->registerJs("var hash = window.location.hash; hash && $('ul.nav a[href=\"' + hash + '\"]').tab('show');"); ?>

</div>
