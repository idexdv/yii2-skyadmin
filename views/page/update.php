<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Page */

$this->params['breadcrumbs'][] = ['label' => $indexTitle, 'url' => $indexAction];
foreach ($model->bloodLine as $item) {
    $this->params['breadcrumbs'][] = [
        'label' => \yii\helpers\StringHelper::truncate($item['title'],35),
        'url' => $indexAction + [$indexSearchId => ['parent_id' => $item['id']]],
    ];
}

$this->title = \yii\helpers\StringHelper::truncate($model->title, 40) . ' (изменение)';
$this->params['breadcrumbs'][] = $this->title;
$this->params['breadcrumbs'][] = ['label' => '(Создать запись)', 'url' => @$createAction, 'encode' => false];
?>
<div class="custom-page-update">

    <?= \yii\bootstrap\Tabs::widget([
        'items' => [
            [
                'label' => 'Общее',
                'content' => $this->render('_form', ['model' => $model]),
                'active' => true,
                'options' => ['id' => 'general'],
            ],
            [
                'label' => 'Фото',
                'content' => $this->render('_image', ['model' => $model]),
                'options' => ['id' => 'image'],
            ],
            [
                'label' => '<i class="fa fa-cogs" style="font-size: 12px;"></i> SEO',
                'encode' => false,
                'content' => $this->render('_seo', ['model' => $model]),
                'options' => ['id' => 'seo'],
            ],
            [
                'label' => '<i class="fa fa-share" style="font-size: 12px;"></i> Перейти на страницу',
                'encode' => false,
                'url' => $model->static_url,
                'linkOptions' => ['target' => '_blank'],
            ],
        ],
        'clientEvents' => [
            'show.bs.tab' => new \yii\web\JsExpression('function(e) { location.hash = e.target.hash; }'),
        ],
    ]) ?>

    <? $this->registerJs("var hash = window.location.hash; hash && $('ul.nav a[href=\"' + hash + '\"]').tab('show');"); ?>

</div>
