<?php

use yii\helpers\Html;
use yii\helpers\StringHelper;
use admin\widgets\GridView2;

/* @var $this yii\web\View */
/* @var $searchModel admin\models\DictionarySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $title;
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="order-index">

    <p>
        <?= Html::a('Создать', ['create', 'type' => $cid], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView2::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'pager' => [
            'maxButtonCount' => 5,
            'prevPageLabel' => '<span class="glyphicon glyphicon-backward"></span>',
            'nextPageLabel' => '<span class="glyphicon glyphicon-forward"></span>',
            'firstPageLabel' => '<span class="glyphicon glyphicon-fast-backward"></span>',
            'lastPageLabel' => '<span class="glyphicon glyphicon-fast-forward"></span>',
            'options' => ['class' => 'pagination center'],
        ],
        'columns' => [
            ['class' => 'yii\grid\CheckboxColumn', 'headerOptions' => ['class' => 'checkbox-column']],
            ['class' => 'admin\widgets\ControlColumn', 'template' => '{active}', 'headerOptions' => ['class' => 'control-column']],
            'customer_name',
            'customer_phone',
            'customer_email',
            'created_at:date',
            'updated_at:date',
            ['class' => 'admin\widgets\ControlColumn', 'headerOptions' => ['class' => 'delete-column'], 'template' => '{delete}'],
        ],
        'tableOptions' => ['class' => 'table table-striped table-bordered td-v-m']
    ]); ?>
</div>
