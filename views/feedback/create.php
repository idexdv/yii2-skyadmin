<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Review */

$this->title = 'Создание - ' . $indexTitle;
$this->params['breadcrumbs'][] = ['label' => $indexTitle, 'url' => $indexAction];
$this->params['breadcrumbs'][] = 'Создание';
?>
<div class="custom-review-create">

    <?= \yii\bootstrap\Tabs::widget([
        'items' => [
            [
                'label' => 'Общее',
                'content' => $this->render('_form', ['model' => $model]),
                'active' => true,
                'options' => ['id' => 'general'],
            ],
        ],
    ]) ?>

</div>
