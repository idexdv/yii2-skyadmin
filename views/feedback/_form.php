<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Dictionary;

/* @var $this yii\web\View */
/* @var $model common\models\Dictionary */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="well">
    <div class="row">

        <?php $form = ActiveForm::begin(['id' => 'update-form']); ?>

            <?= ($_errorHtml = $form->errorSummary($model)) ? '<div class="col-sm-12 text-danger">'.$_errorHtml.'</div>' : '' ?>

            <?= $form->field($model, 'customer_name', ['options' => ['class' => 'col-sm-9']]) ?>

            <?= $form->field($model, 'active', ['options' => ['class' => 'col-sm-3']])->dropDownList(['1' => 'Активен', '0' => 'Выключен']) ?>

            <?= \admin\widgets\form\DynamicForm::widget(['form' => $form, 'model' => $model]) ?>

            <div class="clearfix"></div>
            <div class="form-group col-md-12">
                <?= Html::a('Сохранить и закрыть', null, [
                    'onclick' => 'SaveAndExit(this);',
                    'class' => 'pull-right btn btn-success',
                ]) ?>
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
            </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
