<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\User;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */

$isMyModel = \Yii::$app->personal->isMe($model->id);
?>

<div class="user-form row">
    <div class="col-lg-12">

        <?php $form = ActiveForm::begin(); ?>

    	<div class="row">

            <?= $form
                    ->field($model, 'username', ['options' => ['class' => 'col-sm-4']])
                    ->textInput(['maxlength' => true, 'placeholder' => 'Имя пользователя', 'required' => true]) ?>

            <?= $form
                    ->field($model, 'email', ['options' => ['class' => 'col-sm-5']])
                    ->textInput(['maxlength' => true, 'placeholder' => 'Эл.почта обязательна']) ?>

            <?= $form
                    ->field($model, 'status', ['options' => ['class' => 'col-sm-3']])
                    ->dropDownList([0 => 'Выключен', 10 => 'Активен'], ['disabled' => $isMyModel]) ?>

            <div class="clearfix"></div>

            <?= $form
                    ->field($model, 'role', ['options' => ['class' => 'col-sm-4']])
                    ->dropDownList($model::$roles, ['prompt' => 'Укажите роль', 'disabled' => $isMyModel])->hint($isMyModel ? 'Нельзя менять свою роль' : '') ?>

            <?= $form
                    ->field($model, 'passw_new', ['options' => ['class' => 'col-sm-5']])
        			->textInput([
                        'placeholder' => 'Чтобы задать '.($model->isNewRecord ? '': 'новый ').'пароль, введите его сюда...',
                        'style' => 'border-color: #fb3;'
                        ] + ($model->isNewRecord ? ['required' => true] : []))
                    ->label($model->isNewRecord ? 'Задать пароль' : 'Задать новый пароль') ?>

    		<div class="form-group col-sm-12">
                <hr>
                <?
                    if (!$isMyModel && !$model->isNewRecord) {
                        echo Html::a(
                            'Удалить запись',
                            ['delete', 'id' => $model->id],
                            ['data-confirm' => 'Действительно удалить эту запись навсегда?!', 'class' => 'btn btn-danger pull-right']
                        );
                    }
                ?>
                <?= Html::a('Отмена', ['index'], ['class' => 'btn btn-default']) ?>
                <?= Html::input('submit', 'save', 'Сохранить', ['class' => 'btn btn-primary']) ?>
    		</div>

    	</div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
