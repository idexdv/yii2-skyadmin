<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\User;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index row">

<div class="col-lg-12">

    <p>
        <?= Html::a('Создать пользователя', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'username',
                'header' => 'Имя',
                'format' => 'raw',
                'value' => function($model){
                    return Html::a($model->username, ['update', 'id' => $model['id']]);
                },
            ],
            [
                'attribute' => 'email',
                'format' => 'raw',
                'value' => function($model){
                    return Html::a($model->email, ['update', 'id' => $model['id']]);
                },
            ],
            [
                'attribute' => 'role',
                'filter' => \common\models\User::$roles,
                'value' => function($model, $key){
                    return $model->roleTitle;
                },
                'headerOptions' => ['style' => 'width: 16%; text-align: center;'],
                'contentOptions' => ['style' => 'width: 16%; text-align: center;'],
            ],
            // [
            //     'attribute' => 'status',
            //     'filter' => User::$statusTitles,
            //     'value' => function($model, $key){
            //         return @User::$statusTitles[$model->status];
            //     },
            //     'headerOptions' => ['style' => 'width: 12%; text-align: center;'],
            //     'contentOptions' => ['style' => 'width: 12%; text-align: center;'],
            // ],
            [
                'attribute' => 'created_at',
                'format' => 'raw',
                'value' => function($model, $key){
                    return date('d.m.Y', $model['created_at']);
                },
                'headerOptions' => ['style' => 'width: 12%; text-align: center;'],
                'contentOptions' => ['style' => 'width: 12%; text-align: center;'],
            ],
        ],
    ]); ?>

</div>
</div>