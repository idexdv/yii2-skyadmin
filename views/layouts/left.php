<?php

use common\models\Page;
use common\models\Feedback;
use common\models\Navigator;

$pageCounters = \yii\helpers\ArrayHelper::map(\Yii::$app->getDb()->createCommand('select type, count(*) as cnt from page group by type')->queryAll(), 'type', 'cnt');
array_walk($pageCounters, function(&$i){
    if ($i > 0) $i = ' <span class="pull-right badge" style="margin-top: 2px; font-weight: normal;">' . $i . '</span>';
});

$feedbackCounters = \yii\helpers\ArrayHelper::map(\Yii::$app->getDb()->createCommand('select type, count(*) as cnt from feedback group by type')->queryAll(), 'type', 'cnt');
array_walk($feedbackCounters, function(&$i){
    if ($i > 0) $i = ' <span class="pull-right badge" style="margin-top: 2px; font-weight: normal;">' . $i . '</span>';
});

$navigatorCounters = \yii\helpers\ArrayHelper::map(\Yii::$app->getDb()->createCommand('select type, count(*) as cnt from navigator group by type')->queryAll(), 'type', 'cnt');
array_walk($navigatorCounters, function(&$i){
    if ($i > 0) $i = ' <span class="pull-right badge" style="margin-top: 2px; font-weight: normal;">' . $i . '</span>';
});


$pageItems = [];
foreach (Page::$controllerIds as $id => $type) {
    $pageItems[] = [
        'encode' => false,
        'label' => (@Page::$controllerTitles[$id] ?: 'n/a') . @$pageCounters[$id],
        'icon' => @Page::$controllerIcons[$id] ?: 'file',
        'url' => ['/admin/page/index', 'type' => $type],
    ];
}

$feedbackItems = [];
foreach (Feedback::$controllerIds as $id => $type) {
    $feedbackItems[] = [
        'encode' => false,
        'label' => (@Feedback::$controllerTitles[$id] ?: 'n/a') . @$feedbackCounters[$id],
        'icon' => @Feedback::$controllerIcons[$id] ?: 'envelope',
        'url' => ['/admin/feedback/index', 'type' => $type],
    ];
}

$navigatorItems = [];
foreach (Navigator::$controllerIds as $id => $type) {
    $navigatorItems[] = [
        'encode' => false,
        'label' => (@Navigator::$controllerTitles[$id] ?: 'n/a') . @$navigatorCounters[$id],
        'icon' => @Navigator::$controllerIcons[$id] ?: 'code',
        'url' => ['/admin/navigator/index', 'type' => $type],
    ];
}

?>

<aside class="main-sidebar">

    <section class="sidebar">

        <?
            foreach (@\Yii::$app->params['skyadmin']['adminMenu'] ?: [] as $menu) {
                if (\Yii::$app->personal->can($menu['role'])) {
                    echo dmstr\widgets\Menu::widget([
                        'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                        'items'   => $menu['items'],
                    ]);
                }
            }
        ?>

        <? if (\Yii::$app->personal->can('MANAGER')) echo dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items'   => array_merge([['label' => 'Контент', 'options' => ['class' => 'header']]], $pageItems),
            ]
        ) ?>

        <? if (false && \Yii::$app->personal->can('MANAGER') && $feedbackItems) echo dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items'   => array_merge([['label' => 'Сообщения', 'options' => ['class' => 'header']]], $feedbackItems),
            ]
        ) ?>

        <? if (\Yii::$app->personal->can('ADMIN')) echo dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items'   => array_merge([['label' => 'Навигатор', 'options' => ['class' => 'header']]], $navigatorItems),
            ]
        ) ?>

        <? if (\Yii::$app->personal->can('ADMIN')) echo dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Конфигурация', 'options' => ['class' => 'header']],
                    ['label' => 'Пользователи', 'icon' => 'users', 'url' => ['/admin/user']],
                    ['label' => 'Настройки', 'icon' => 'cogs', 'url' => ['/admin/default/settings']],
                    ['label' => 'Обслуживание', 'items' => array_merge([
                            ['label' => 'Удаление кеш-файлов', 'icon' => 'trash-o', 'url' => ['/admin/fixhelper/clean-cache']],
                            ['label' => 'Удаление лишних фото', 'icon' => 'trash-o', 'url' => ['/admin/fixhelper/image-clean-up']],
                            ['label' => 'Файл-менеджер', 'icon' => 'file-o', 'url' => ['/admin/default/files']],
                        ], @\Yii::$app->params['skyadmin']['supportMenu'] ?: []),
                    ],
                ],
            ]
        ) ?>

    </section>

</aside>
