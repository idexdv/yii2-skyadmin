<?php

namespace admin\controllers;

use Yii;
use yii\web\Controller;

class NavigatorController extends Controller
{
    use \admin\components\ControllerTrait;
    use \admin\components\ControllerCUDTrait;
    use \admin\components\ControllerBulkActionTrait;

    private $type;
    // private $baseSlug = 'navigator';
    private $baseClass = \common\models\Navigator::class;
    private $searchClass = \admin\models\NavigatorSearch::class;
    private $rootController = '/admin/navigator';

    /**
     * List feed by type and query string.
     * Used for Select2 search input in Navigator page
     * @param  integer  $type   Type of a page
     * @param  string   $q      Query string (search input)
     * @return json             Result of the search
     */
    public function actionListFeed($type, $q)
    {
        $rows = \common\models\Page::find()
                        ->select('id,title,static_url,type')
                        ->andWhere(['type' => $type])
                        ->andWhere(['not', ['static_url' => '']])
                        ->andWhere(['like', 'title', $q])
                        ->indexBy('id')
                        ->limit(10)
                        ->asArray()->all();

        $items = [];
        foreach ($rows as $key => $item) {
            $items[$item['id']] = [
                'target' => \common\models\Page::$controllerIds[$item['type']] . ':' . $item['id'],
                'value' => html_entity_decode($item['title']),
                'url' => $item['static_url'],
            ];
        }

        return $this->asJson($items);
    }
}

