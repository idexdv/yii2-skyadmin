<?php

namespace admin\controllers;

use Yii;
use yii\web\Controller;
use common\models\Feedback;
use admin\models\FeedbackSearch;

class FeedbackController extends Controller
{
    use \admin\components\ControllerTrait;
    use \admin\components\ControllerCUDTrait;
    use \admin\components\ControllerBulkActionTrait;

    private $type;
    private $baseClass = Feedback::class;
    private $searchClass = FeedbackSearch::class;
    private $rootController = '/admin/feedback';

    public function actionIndex()
    {
        $searchModel = new $this->searchClass();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->render('index', [
            'title' => 'Заказы',
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        return $this->actionUpdate(null, true);
    }

    public function actionUpdate($id, $__createMode = false)
    {
        if ($id === null && $__createMode === true) {
            $model = new $this->baseClass();
        } else {
            $model = $this->findModel($id);
        }

        if ($model->load(\Yii::$app->request->post()) && $model->preProcess() && $model->save()) {
            if (!isset($_POST['exitmode']))
                return $this->redirect(["{$this->rootController}/update", 'id' => $model->id]);    
            else {
                return $this->redirect(["{$this->rootController}/index"]);
            }
        } else {
            return $this->render($__createMode ? 'create' : 'update', [
                'model' => $model,
                'indexTitle' => 'Заказы',
                'indexAction' => ["{$this->rootController}/index"],
                'indexSearchId' => (new $this->searchClass())->formName(),
            ]);
        }
    }
}
