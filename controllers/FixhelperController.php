<?php

namespace admin\controllers;

use Yii;
use yii\helpers\Url;
use yii\web\Controller;

class FixhelperController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionCleanCache()
    {
        $errors = [];

        $result = (new \admin\components\ConsoleHelper)->runAction('cache/flush-all', $output);
        if (@$result != 0) {
            $errors[] = ['danger', 'При выполнении команды «Очистка кеша», возникла ошибка.'];
        }
        $folders = glob(\Yii::getAlias('@frontend/runtime/cache/*'), GLOB_ONLYDIR);
        foreach ($folders as $folder) {
            \yii\helpers\FileHelper::removeDirectory($folder);
        }
        $folders = glob(\Yii::getAlias('@webroot/assets/*'), GLOB_ONLYDIR);
        foreach ($folders as $folder) {
            \yii\helpers\FileHelper::removeDirectory($folder);
        }
        \Yii::$app->session->setFlash('success', 'Очистка кеш-файлов и web-assets файлов выполнена успешно.');
        foreach ($errors as $status => $comment) \Yii::$app->session->addFlash($status, $comment);

        return $this->redirect(\Yii::$app->request->referrer ?: ['/admin']);
    }

    public function actionImageCleanUp()
    {
        $errors = [];

        /**
         * Delete image cache
         */
        \yii\helpers\FileHelper::removeDirectory(\Yii::getAlias('@webroot/img/cache'));

        \Yii::$app->session->setFlash('success', 'Очистка лишних пользовательских фотографий и кеша изображений выполнена успешно.');
        foreach ($errors as $status => $comment) \Yii::$app->session->addFlash($status, $comment);

        return $this->redirect(\Yii::$app->request->referrer ?: ['/admin']);
    }
}

