<?php

namespace admin\controllers;

use Yii;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\UploadedFile;
use admin\models\FileUpload;
use common\models\Image;

class UtilsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'img-upload' => ['post'],
                    'img-sort' => ['post'],
                ],
            ],
        ];
    }

    public function actionImgUpload()
    {
        $uploadForm = new FileUpload();

        if (isset(\Yii::$app->request->queryParams['type'])) {
            /**
             * CKEditor image upload processing.
             */
            $uploadForm->imageFile = UploadedFile::getInstanceByName('upload');
            if ($uploadForm->upload(true)) {
                if ($image = Image::addNew($uploadForm->filepath, null)) {

                    list($imgWidth, $imgHeight) = getimagesize(\Yii::getAlias('@webroot') . $image->filename);
                	$maxWidth  = (int) \Yii::$app->st->defaultsImageEditorWidth_integer(400);
                	$maxHeight = (int) \Yii::$app->st->defaultsImageEditorHeight_integer(300);

                    if ($imgWidth > 0 && $imgHeight > 0 && $maxWidth > 0 && $maxHeight > 0) {
	                	$aspectRatio = (int) $imgWidth / (int) $imgHeight;
	                    $outWidth  	 = min($maxWidth, $imgWidth);
	                	$outHeight   = $outWidth / $aspectRatio;
	                	if ($outHeight > $maxHeight) {
	                		$outHeight = $maxHeight;
	                		$outWidth  = $outHeight * $aspectRatio;
	                	}
	                	$outWidth  = (int)$outWidth;
	                	$outHeight = (int)$outHeight;
					}

                    if ($fn = @$_GET['CKEditorFuncNum']) {
                        return Html::script(new \yii\web\JsExpression("window.parent.CKEDITOR.tools.callFunction('{$fn}', '{$image->filename}', '')"));
                    } else {
                        return $this->asJson([
                            'uploaded' 	=> 1,
                            'fileName' 	=> basename($image->filename),
                            'url' 		=> $image->filename,
                            'width' 	=> @$outWidth,
                            'height' 	=> @$outHeight,
                        ]);
                    }
                } else {
                    return $this->asJson([
                        'uploaded' => 0,
                        'error' => ['message' => 'Не получилось загрузить файл этим способом.'],
                    ]);
                }
                $uploadForm->cleanUp();
            }
        } else if ($uploadForm->load(Yii::$app->request->post())) {
            /**
             * Default image upload processing.
             */
            $uploadForm->imageFile = UploadedFile::getInstance($uploadForm, 'imageFile');
            if ($uploadForm->upload()) {
                $model = $uploadForm->getModel();
                if ($model->importImage($uploadForm->filepath)) {
                    return $this->asJson(true);
                }
                $uploadForm->cleanUp();
            }
        }
        return $this->asJson(['error' => 'Ошибка при загрузке файла изображения.']);
    }

    public function actionImgSort()
    {
        $objectSort = \Yii::$app->request->post('sort');
        $dynModel = \yii\base\DynamicModel::validateData(compact('objectSort'), [
            ['objectSort', 'each', 'rule' => ['integer']],
        ]);
        if ($dynModel->hasErrors()) {
            return $this->asJson(false);
        }
        foreach ($objectSort as $key => $imageId) {
            if ($imgModel = Image::findOne(['id' => $imageId])) {
                $imgModel->ordering = ($key + 1);
                if ($imgModel->save() === false) {
                    return $this->asJson(false);
                }
            }
        }
        return $this->asJson(true);
    }

    public function actionImgDelete($id)
    {
        if ($imgModel = Image::findOne(['id' => (int) $id])) {
            $imgModel->delete();
        }
        return $this->redirect(\Yii::$app->request->referrer ? \Yii::$app->request->referrer . '#image' : ['/admin']);
    }

    private $allowedMScope = [
        'Dictionary',
        'Page',
        'Product',
    ];

    public function actionImgDeleteModel($mid, $prop, $mscope)
    {
        if (in_array($mscope, $this->allowedMScope)) {
            $modelClass = '\\common\\models\\' . $mscope;
            if ($model = $modelClass::findOne($mid)) {
                $_prop = $model->prop;
                unset($_prop[$prop]);
                $model->prop = $_prop;
                $model->save();
            }
        }
        return $this->redirect(\Yii::$app->request->referrer ? \Yii::$app->request->referrer : ['/admin']);
    }
}

