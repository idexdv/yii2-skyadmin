<?php

namespace admin\controllers;

use Yii;
use yii\web\Controller;

class PageController extends Controller
{
    use \admin\components\ControllerTrait;
    use \admin\components\ControllerCUDTrait;
    use \admin\components\ControllerBulkActionTrait;

    private $type;
    // private $baseSlug = 'page';
    private $baseClass = \common\models\Page::class;
    private $searchClass = \admin\models\PageSearch::class;
    private $rootController = '/admin/page';
}

