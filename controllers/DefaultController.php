<?php

namespace admin\controllers;

use yii\web\Controller;
use common\models\Setting;

class DefaultController extends Controller
{
	use \admin\components\ControllerTrait;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'img-upload' => ['post'],
                    'img-sort' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionFiles()
    {
        return $this->render('files');
    }

    public function actionSettings()
    {
        if (\Yii::$app->request->isPost) {
            $post = \Yii::$app->request->post('S');
            if (is_array($post)) {
                foreach ($post as $idk => $value) {
                    if (Setting::find()->where(['idk' => $idk])->exists()) {
                        \Yii::$app->st->$idk($value, true);
                    }
                }
            }
        }

        return $this->render('settings');
    }
}
