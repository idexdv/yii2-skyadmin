<?php

/*
 * This file is part of the admin package.
 *
 * (c) Dmitry Vikharev <idexdv@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
 
namespace admin\widgets;

use Yii;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use admin\models\ImageForm;

class ImageHelper extends \yii\base\Widget
{
	public $model;
	public $mainImage;
	public $mainTitle = 'Главное изображение';
	public $generalImage;

	public function run()
	{
		if($this->model === null) return '';

		echo Html::tag('div', '', ['style' => 'border-top: 1px solid #ccc; margin: 16px 0;', 'class' => 'visible-xs visible-sm']);
		
		echo Html::beginTag('div', ['id' => 'image-helper']);

		if($this->mainImage !== null) {
			echo '<label>'.$this->mainTitle.'</label>';
			$this->renderMain();
			echo '<hr style="border-top: 1px solid #ccc;">';
		}

		if($this->generalImage !== null) {
			echo '<label>'.Yii::t('app', 'Галлерея').'</label>';
			if(!$this->renderGeneral()) echo Html::tag('p', Yii::t('app', 'Нет изображений в галлерее'), ['class' => 'text-center']);
		}

		echo Html::endTag('div');

		$this->view->registerJs('var formIsChanged = false;', \yii\web\View::POS_BEGIN);
		$this->view->registerJs('setTimeout(function(){ $("#update-form").on(\'change.yii\',function(e){
			$(\'#image-helper form\').addClass(\'disabled\');
			$(\'#image-helper form\').append(\'<div class="warning"><span>В форме присутствуют не сохраненные записи!<br>Необходимо их сохранить, либо перезагрузить страницу.</span></div>\');
		}); }, 1000);');

	}

	public function renderMain()
	{
		$mainImg = $this->mainImage;
		$hasImage = false;

		echo '<div class="row" style="padding: 8px; position: relative;">';
			$hasImage = $this->renderImage($this->model->$mainImg, true);
	
			$form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'class' => 'image-gallery']]);
			
			if(!$hasImage) {
				echo '<div class="col-xs-12 well">';
				echo $form->field($imageForm = new ImageForm, 'imageFile')->fileInput();
				echo $form->field($imageForm, 'isMainImage')->hiddenInput(['value' => 1])->label(false);
				echo Html::submitButton('Загрузить', ['class' => 'btn btn-sm btn-primary form-control']);
				echo '</div>';
				echo Html::tag('p', Yii::t('app', 'Изображение не задано'), ['class' => 'text-center']);
			} else {
				// echo Html::tag('p', Yii::t('app', 'Чтобы заменить, удали текущее фото.'), ['class' => 'text-center', 'style' => 'font-size: 14px; margin-top: -8px;']);
			}

			ActiveForm::end();
		echo '</div>';

		return $hasImage;
	}

	public function renderGeneral()
	{
		$mainImg = $this->generalImage;
		$hasImage = false;

		echo '<div class="row general-image-gallery" style="padding: 8px; position: relative; overflow: auto;">';

			$hasImage = $this->renderImage($this->model->$mainImg, false);

			$form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'class' => 'image-gallery']]);

			echo '<div class="col-xs-12 well">';
			echo $form->field(new ImageForm, 'imageFiles[]')->fileInput(['multiple' => true, 'accept' => 'image/*']);
			echo Html::submitButton('Загрузить', ['class' => 'btn btn-sm btn-primary form-control']);
			echo '</div>';

			ActiveForm::end();
		echo '</div>';

		return $hasImage;
	}	

	public function renderImage($images, $isSingle)
	{
		if(empty($images)) return false;

		if(!is_array($images)) $images = [$images];

		$_count = count($images);

		if($_count > 1) {
			\yii\jui\JuiAsset::register($this->view);
			$this->view->registerJs(" $('.general-image-gallery').sortable({
				// tolerance: 'pointer',
				stop: function( event, ui ) {
					var sorted = $('.general-image-gallery').sortable('serialize', {key: 'sort[]'});
					$.post('".Url::current()."', {order: sorted});
				}
			}); "); 

		}

		foreach ($images as $key => $image) {
				if($isSingle)
					echo '<div class="col-xs-6 col-sm-4 col-md-3 image-card" id="pimg-'.$image->id.'">';
				else
					echo '<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 image-card" id="pimg-'.$image->id.'">';

			if($_count > 1)
				echo '<a href="'.$image->url.'" data-lightbox="pic-gallery">'.
					Html::tag('div', '', ['style' => "background-image: url( {$image->thumbnail} );", 'class' => 'img-background']).
					'</a>';
			else
				echo '<a href="'.$image->url.'" data-lightbox="pic-gallery">'.
					Html::tag('div', '', ['style' => "background-image: url( {$image->thumbnail} );", 'class' => 'img-background']).
					'</a>';
				// echo '<a href="'.$image->url.'" data-lightbox="pic-gallery">'.Html::img($image->thumbnail, ['class' => 'img-responsive']).'</a>';

			echo '<a class="delete-image" href="'.Url::to(['', 'id' => $this->model->id, 'delete_image_id' => $image->id]).'" data-confirm="Действительно удалить?" title="Удалить изображение"><i class="fa fa-trash fa-2x"></i></a>';
			if($_count > 1) echo '<div class="pic-counter">'.($key + 1).'</div>';
			echo '</div>';
		}

		return true;
	}
}
