<?php

/*
 * This file is part of the admin package.
 *
 * (c) Dmitry Vikharev <idexdv@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace admin\widgets;

use Yii;
use Closure;
use yii\helpers\Html;
use yii\helpers\Url;

class CallbackColumn extends \yii\grid\Column
{
	public $header = '<span class="glyphicon glyphicon-phone-alt"></span>';
	public $headerOptions = ['class' => 'callback-column'];
	public $controller;
	public $template = '{callback_button} {callback_data}';
	public $buttons = [];
	public $visibleButtons = [];
	public $urlCreator;
	public $buttonOptions = [];

	public function init()
	{
		parent::init();
		$this->initDefaultButtons();
	}

	protected function initDefaultButtons()
	{
		if (!isset($this->buttons['callback_button'])) {
			$this->buttons['callback_button'] = function ($url, $model, $key) {
				$options = array_merge([
					'title' => $model['callback_type'] == 0 ? Yii::t('app', 'Callback is undefined (Set callback in company\'s profile)') : ($model['callback_type'] < 0 ? Yii::t('app', 'Callback is inactive (Press to turn it on)') : Yii::t('app', 'Callback is active (Press to turn it off)')),
					'aria-label' => Yii::t('app', 'Callback method'),
					'data-pjax' => '0',
					'class' => 'btn ' . (($model['callback_type'] <= 0) ? 'btn-default' : 'btn-success'),
				], $this->buttonOptions);
				
				switch (abs($model['callback_type'])) {
					case '1':
						$icon = 'glyphicon glyphicon-globe';
						break;
					
					case '2':
						$icon = 'glyphicon glyphicon-envelope';
						break;
					
					default:
						$icon = 'glyphicon glyphicon-question-sign';
						break;
				}

				return Html::a('<span class="' . $icon . '"></span>', ($model['callback_type'] != 0) ? $url : null, $options);
			};
		}
		if (!isset($this->buttons['callback_data'])) {
			$this->buttons['callback_data'] = function ($url, $model, $key) {
				$options = array_merge([
					'title' => Yii::t('app', 'Callback data'),
					'aria-label' => Yii::t('app', 'Callback data'),
					'class' => ($model['callback_type'] <= 0) ? 'disabled' : '',
				], $this->buttonOptions);

				$method = (abs($model['callback_type']) == 2) ? 'mailto:' : '';

				return Html::a($model['callback_data'], $method . $model['callback_data'], $options);
			};
		}
	}

	public function createUrl($action, $model, $key, $index)
	{
		if (is_callable($this->urlCreator)) {
			return call_user_func($this->urlCreator, $action, $model, $key, $index);
		} else {
			$params = is_array($key) ? $key : ['id' => (string) $key];
			$params[0] = $this->controller ? $this->controller . '/' . $action : $action;

			return Url::toRoute($params);
		}
	}

	protected function renderDataCellContent($model, $key, $index)
	{
		return preg_replace_callback('/\\{([\w\-\/]+)\\}/', function ($matches) use ($model, $key, $index) {
			$name = $matches[1];

			if (isset($this->visibleButtons[$name])) {
				$isVisible = $this->visibleButtons[$name] instanceof \Closure
					? call_user_func($this->visibleButtons[$name], $model, $key, $index)
					: $this->visibleButtons[$name];
			} else {
				$isVisible = true;
			}

			if ($isVisible && isset($this->buttons[$name])) {
				$url = $this->createUrl($name, $model, $key, $index);
				return call_user_func($this->buttons[$name], $url, $model, $key);
			} else {
				return '';
			}
		}, $this->template);
	}
}
