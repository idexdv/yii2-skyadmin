<?php
namespace admin\widgets;

use yii\grid\GridView;
use yii\base\InvalidConfigException;

/**
* Custom grid view.
*/
class GridView2 extends \yii\base\BaseObject
{
    public $options = [];
    public $layout = "{search}\n{pagecontrol}\n{grid}\n{bulkcontrol}";
    private $widgetId;

    public function init()
    {
        parent::init();
        if (empty($this->options['dataProvider'])) {
            throw new InvalidConfigException('The "dataProvider" property must be set.');
        }
    }

    public static function widget($config = [])
    {
        return (new self(['options' => $config]))->run();
    }

    public function run()
    {
        $content = preg_replace_callback('/{\\w+}/', function ($matches) {
            $content = $this->renderSection($matches[0]);
            return $content === false ? $matches[0] : $content;
        }, $this->layout);

        return $content;
    }

    public function renderSection($name)
    {
        switch ($name) {
            case '{search}':
                return $this->renderSearch();
            case '{pagecontrol}':
                return $this->renderPageControl();
            case '{grid}':
                return $this->renderGrid();
            case '{bulkcontrol}':
                return $this->renderBulkControl();
            default:
                return false;
        }
    }

    private function renderPageControl()
    {
        return \admin\widgets\GridPager::widget(['dataProvider' => $this->options['dataProvider']]);
    }

    private function renderGrid()
    {
        /**
         * OB_START() is for widget->run() echoes html content, instead of to return it as a string;
         */
        ob_start();
        ob_implicit_flush(false);

        $widget = GridView::begin($this->options);
        $this->widgetId = $widget->id;
        $widget->run();

        return ob_get_clean();
    }

    private function renderBulkControl()
    {
        return BulkAction::widget(['sourceId' => "#{$this->widgetId}"]);
    }

    private function renderSearch()
    {
        return \Yii::$app->getView()->render('@admin/widgets/form/IndexSearchForm', ['searchModel' => $this->options['filterModel']]);
    }
}
