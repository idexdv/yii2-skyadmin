<?php

/*
 * This file is part of the admin package.
 *
 * (c) Dmitry Vikharev <idexdv@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace admin\widgets;

use Yii;
use Closure;
use yii\helpers\Html;
use yii\helpers\Url;

class ControlColumn extends \yii\grid\Column
{
	public $header = '<span class="fa fa-wrench"></span>';
	public $headerOptions = ['class' => 'action-column'];
	public $contentOptions = ['class' => 'action-column'];
	public $controller;
	public $template = '{active} {payed}';
	public $buttons = [];
	public $visibleButtons = [];
	public $urlCreator;
	public $buttonOptions = [];
	public $actionOptions = [];

	public function init()
	{
		parent::init();
		$this->initDefaultButtons();
	}

	protected function initDefaultButtons()
	{
		if (!isset($this->buttons['payed'])) {
			$this->buttons['payed'] = function ($url, $model, $key) {
				$options = array_merge([
					'title' => Yii::t('app', 'Payed (Press if not payed)'),
					'aria-label' => Yii::t('app', 'Payed'),
					'data-pjax' => '0',
					'class' => 'btn btn-sm ' . ($model['payed'] ? 'btn-success' : 'btn-default'),
				], $this->buttonOptions);
				return Html::a('<span class="fa fa-rub"></span>', $url, $options);
			};
		}
		if (!isset($this->buttons['active'])) {
			$this->buttons['active'] = function ($url, $model, $key) {
				$options = array_merge([
					'title' => Yii::t('app', 'Active (press to disactivate)'),
					'aria-label' => Yii::t('app', 'Active'),
					'data-pjax' => '0',
					// 'class' => 'btn btn-sm ' . ($model['active'] ? 'btn-success' : 'btn-default'),
					'class' => 'btn btn-sm ' . (!$model['active'] ? 'btn-default' : ($model['active'] == -1 ? 'btn-warning' : 'btn-success')),
				], $this->buttonOptions);
				return Html::a('<span class="fa fa-power-off"></span>', $url, $options);
			};
		}
		if (!isset($this->buttons['front_visible'])) {
			$this->buttons['front_visible'] = function ($url, $model, $key) {
				$options = array_merge([
					// 'title' => Yii::t('app', 'Visible (press to hide)'),
					'aria-label' => Yii::t('app', 'Visible'),
					'data-pjax' => '0',
					'class' => 'btn btn-sm ' . (!$model['front_visible'] ? 'btn-default' : ($model['front_visible'] == -1 ? 'btn-warning' : 'btn-success')),
				], $this->buttonOptions);
				return Html::a('<span class="fa fa-off"></span>', $url, $options);
			};
		}
		if (!isset($this->buttons['available'])) {
			$this->buttons['available'] = function ($url, $model, $key) {
				$options = array_merge([
					// 'title' => Yii::t('app', 'Visible (press to hide)'),
					'aria-label' => Yii::t('app', 'Visible'),
					'data-pjax' => '0',
					'class' => 'btn btn-sm ' . (!$model['available'] ? 'btn-default' : ($model['available'] == -1 ? 'btn-warning' : 'btn-success')),
				], $this->buttonOptions);
				return Html::a('<span class="fa fa-eye-open"></span>', $url, $options);
			};
		}
		if (!isset($this->buttons['draft'])) {
			$this->buttons['draft'] = function ($url, $model, $key) {
				$options = array_merge([
					'title' => 'Запись - Черновик (Нажать, чтобы Принять)',
					'aria-label' => 'Черновик',
					'data-pjax' => '0',
					'class' => 'btn btn-sm btn-warning',
				], $this->buttonOptions);
				return Html::a('<span class="fa fa-thumbs-up"></span>', $url, $options);
			};
		}
		if (!isset($this->buttons['update'])) {
			$this->buttons['update'] = function ($url, $model, $key) {
				$options = array_merge([
					'title' => 'Редактировать',
					'aria-label' => 'Редактировать',
					'data-pjax' => '0',
					'class' => 'btn btn-sm btn-primary',
				], $this->buttonOptions);
				return Html::a('<span class="fa fa-pencil"></span>', $url, $options);
			};
		}
		if (!isset($this->buttons['clone'])) {
			$this->buttons['clone'] = function ($url, $model, $key) {
				$options = array_merge([
					'title' => 'Копировать',
					'aria-label' => 'Копировать',
					'data-pjax' => '0',
					'class' => 'btn btn-sm btn-default',
				], $this->buttonOptions);
				return Html::a('<span class="fa fa-clone"></span>', $url, $options);
			};
		}
		if (!isset($this->buttons['delete'])) {
			$this->buttons['delete'] = function ($url, $model, $key) {
				return '<a class="btn btn-sm btn-danger" href="'.$url.'" title="Удалить" aria-label="Удалить" data-confirm="Вы уверены, что хотите удалить этот элемент?" data-method="post" data-pjax="0"><span class="fa fa-trash"></span></a>';
			};
		}
	}

	public function createUrl($action, $model, $key, $index)
	{
		if (is_callable($this->urlCreator)) {
			return call_user_func($this->urlCreator, $action, $model, $key, $index);
		} else {
			$params = is_array($key) ? $key : ['id' => (string) $key];
			$params[0] = $this->controller ? $this->controller . '/' . $action : $action;

			return Url::toRoute($params);
		}
	}

	protected function renderDataCellContent($model, $key, $index)
	{
		return preg_replace_callback('/\\{([\w\-\/]+)\\}/', function ($matches) use ($model, $key, $index) {
			$name = $matches[1];

			if (isset($this->visibleButtons[$name])) {
				$isVisible = $this->visibleButtons[$name] instanceof \Closure
					? call_user_func($this->visibleButtons[$name], $model, $key, $index)
					: $this->visibleButtons[$name];
			} else {
				$isVisible = true;
			}

			if ($isVisible && isset($this->buttons[$name])) {
				$url = $this->createUrl(@$this->actionOptions[$name]['actionLink'] ?: $name, $model, $key, $index);
				return call_user_func($this->buttons[$name], $url, $model, $key);
			} else {
				return '';
			}
		}, $this->template);
	}
}
