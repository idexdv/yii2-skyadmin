<?php

/*
 * This file is part of the admin package.
 *
 * (c) Dmitry Vikharev <idexdv@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace admin\widgets;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Url;

class GridPager extends Widget
{
	public 	$dataProvider;
	public 	$defaultPageSize = 50;
	protected $pageSizeParam = 'per-page';
	protected $pageSizes = [20 => 20, 50 => 50, 100 => 100, 200 => 200, 300 => 300];
	protected $options;
	protected $param = [];

	public function run()
	{
		$perPageValue = Yii::$app->request->get($this->pageSizeParam, $this->defaultPageSize);
		$this->dataProvider->pagination->pageSize = $perPageValue;

		$getParams = Yii::$app->request->queryParams;
		unset($getParams[$this->pageSizeParam]);
		unset($getParams['_pjax']);

		$link = Url::toRoute([''] + $getParams ?: []) . (empty($getParams) ? '?' : '&') . $this->pageSizeParam . '=';

		$this->view->registerJs(<<<END
			$('#{$this->id}').change(function(){
				location.href = "{$link}" + $(this).val();
			});
END
);

		return Html::tag('span', 'Записей на странице '.Html::dropDownList($this->pageSizeParam, $perPageValue, $this->pageSizes, ['id' => $this->id]), ['class' => 'grid-pager pull-right']).
			'<div class="clearfix"></div>';
	}
}
