<?php

namespace admin\widgets\form;

use Yii;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\models\User;
use common\models\Dictionary;

/**
* Dynamic Form widget
*/
class DynamicForm extends \yii\base\Widget
{
    public $form;
    public $model;
    public $bulkMode = false;
    public $overrideFields;
    public $seoMode = null;
    public $seoFieldPrefix = 'seo';
    public $extraFieldPrefix = 'prop';
    public $enableCkEditor = false;

    public function run()
    {
        if ($this->seoMode !== true) {
            if (property_exists($this->model, 'formFields') || method_exists($this->model, 'getFormFields')) {
                foreach ($this->model->formFields as $options) if (@$options['name'] && @$options['type'] && !in_array($options['name'], @$this->overrideFields['except'] ?: [])) {
                    if (empty($options['fieldOptions']['class'])) $options['fieldOptions']['class'] = 'col-sm-12';
                    if (is_null(@$options['typesExcept']) || !in_array($this->model->type, $options['typesExcept']))
                    if (is_null(@$options['typesOnly']) || in_array($this->model->type, $options['typesOnly'])) {
                        if ($renderMethod = 'fieldRender' . \yii\helpers\Inflector::camelize($options['type'])) {
                            if ($this->bulkMode === true) { unset($options['inputOptions']['required']); }
                            $this->$renderMethod($options);
                        }
                    }
                }
            }

            if (property_exists($this->model, 'extraFormFields') || method_exists($this->model, 'getExtraFormFields')) {
                foreach ($this->model->extraFormFields as $options) if (@$options['name'] && @$options['type']) {
                    if (empty($options['fieldOptions']['class'])) $options['fieldOptions']['class'] = 'col-sm-12';
                    if (is_null(@$options['typesExcept']) || !in_array($this->model->type, $options['typesExcept']))
                    if (is_null(@$options['typesOnly']) || in_array($this->model->type, $options['typesOnly'])) {
                        $options['label'] = @$options['label'] ?: \yii\helpers\Inflector::humanize("{$this->extraFieldPrefix} {$options['name']}");
                        $options['name'] = "{$this->extraFieldPrefix}[{$options['name']}]";
                        if ($renderMethod = 'fieldRender' . \yii\helpers\Inflector::camelize($options['type'])) {
                            if ($this->bulkMode === true) { unset($options['inputOptions']['required']); }
                            if (@$options['widgetClass']) {
                                (new $options['widgetClass']($this))->$renderMethod($options);
                            } else {
                                $this->$renderMethod($options);
                            }
                        }
                    }
                }
            }
        }
        if ($this->seoMode !== false) {
            if (property_exists($this->model, 'seoFields') || method_exists($this->model, 'getSeoFields')) {
                foreach ($this->model->seoFields as $options) if (@$options['name'] && @$options['type']) {
                    if (empty($options['fieldOptions']['class'])) $options['fieldOptions']['class'] = 'col-sm-12';
                    if (is_null(@$options['typesExcept']) || !in_array($this->model->type, $options['typesExcept']))
                    if (is_null(@$options['typesOnly']) || in_array($this->model->type, $options['typesOnly'])) {
                        $options['label'] = @$options['label'] ?: \yii\helpers\Inflector::humanize("{$this->seoFieldPrefix} {$options['name']}");
                        $options['name'] = "{$this->seoFieldPrefix}[{$options['name']}]";
                        if ($renderMethod = 'fieldRender' . \yii\helpers\Inflector::camelize($options['type'])) {
                            if ($this->bulkMode === true) { unset($options['inputOptions']['required']); }
                            $this->$renderMethod($options);
                        }
                    }
                }
            }
        }
        if ($this->enableCkEditor) $this->registerCkEditor();
        $this->registerJs();
    }

    public function fieldRenderTextInput($options)
    {
        $field = $this->form
                ->field($this->model, $options['name'], ['options' => @$options['fieldOptions'] ?: []])
                ->textInput(@$options['inputOptions'] ?: []);
        if (@$options['label'] !== null) $field = $field->label($options['label']);
        echo $field;
    }

    public function fieldRenderHidden($options)
    {
        echo Html::activeInput('hidden', $this->model, $options['name']);
   }

    public function fieldRenderPriceInput($options)
    {
        if (!@$options['type']) $options['type'] = 'decimal';
        $options['inputOptions'] = ['title' => 'Введите значение'] + (@$options['inputOptions'] ?: []);
        $field = $this->form
                ->field($this->model, $options['name'], ['options' => @$options['fieldOptions'] ?: []])
                ->widget(\kartik\number\NumberControl::classname(), [
                    'maskedInputOptions' => [
                        'allowMinus' => false
                    ],
                    'displayOptions' => $options['inputOptions'],
                    // 'options' => [
                    // ],
                ]);
        if (@$options['label'] !== null) $field = $field->label($options['label']);
        echo $field;
    }

    public function fieldRenderUrlInput($options)
    {
        $options['inputOptions']['type'] = 'url';
        $optionName = $options['name'];
        if (isset($this->model->$optionName))
            $template = '{label}<div class="input-group"><span class="input-group-addon"><a href="' . $this->model->$optionName . '" target="_blank"><i class="fa fa-globe"></i>&nbsp;Перейти на страницу</a></span>{input}</div>{error}';
        else
            $template = '{label}{input}{error}';

        $field = $this->form
                ->field($this->model, $options['name'], ['options' => @$options['fieldOptions'] ?: [], 'template' => $template])
                ->textInput(@$options['inputOptions'] ?: []);
        if (@$options['label'] !== null) $field = $field->label($options['label']);
        echo $field;
    }

    public function fieldRenderTextArea($options)
    {
        if (isset($options['inputOptions']['data-editor-height'])) {
            $this->enableCkEditor = true;
            $options['inputOptions']['style'] = @$options['inputOptions']['style'] .
                                                ' min-height: ' . (100 + $options['inputOptions']['data-editor-height']) . 'px;';
        }

        $field = $this->form
                ->field($this->model, $options['name'], ['options' => @$options['fieldOptions'] ?: []])
                ->textArea(@$options['inputOptions'] ?: []);
        if (@$options['label'] !== null) $field = $field->label($options['label']);
        echo $field;
   }

   public function registerCkEditor()
   {
        $uploadUrl = Url::to(['/admin/utils/img-upload', 'type' => 'Images']);
        $browseUrl = Url::to(['/admin/utils/img-browse', 'type' => 'Images']);
        \plugins\assets\CkeditorAsset::register($this->view);
        $this->view->registerJs("
            $('[data-styled-editor]').each(function(i, el){
                let height = $(el).data('editor-height');
                if (height == undefined) height = 140;
                CKEDITOR.config.height = height;
                CKEDITOR.config.filebrowserUploadUrl = '{$uploadUrl}';
                CKEDITOR.config.filebrowserBrowseUrl = false; //'{$browseUrl}';
                CKEDITOR.config.allowedContent = {
                    script: true,
                    $1: {
                        // This will set the default set of elements
                        elements: CKEDITOR.dtd,
                        attributes: true,
                        styles: false,
                        classes: true
                    }
                };
                CKEDITOR.config.disallowedContent = 'img[width,height],font,font[color],[align,lang]';
                CKEDITOR.replace($(el).prop('id'));
            });
        ", \yii\web\View::POS_READY, 'dynamic-form-ckeditor');
    }

    public function renderMetaSeo()
    {
        $seoFields = [
            $this->fieldRenderTextInput(['name' => 'Seo[meta_keywords]', 'label' => 'SEO Keywords', ])
        ];
    }

    public function fieldRenderImage($options)
    {
        $html = '';

        preg_match('|\[([a-z_]*)\]|', $options['name'], $m);
        $propName = @$m[1];
        if ($imageFilename = Html::getAttributeValue($this->model, $options['name'])) {
            list($iw, $ih) = @getimagesize(\Yii::getAlias('@webroot') . $imageFilename);
            $imgWH = "{$iw}x{$ih} px";
            $html .= Html::a(
                        Html::img($imageFilename, ['class' => 'extra-image img-thumbnail']),
                        $imageFilename,
                        ['data-lightbox' => $options['name'], 'data-title' => "{$options['label']}, {$imgWH}", 'title' => 'Открыть просмотр']
                    ) . '<br>';
            $html .= Html::a(
                        "Удалить изображение ({$imgWH})",
                        ['/admin/utils/img-delete-model', 'mid' => $this->model->id, 'prop' => $propName, 'mscope' => $this->model->formName()],
                        ['style' => 'margin: 10px 0 0; border-bottom: 1px dashed #444; color: #444; text-align: left; display: inline-block;', 'title' => 'Удалить изображение']);
            $html .= $this->form->field($this->model, $options['name'], ['template' => '{input}'])->hiddenInput();
        } else {
            $html .= $this->form->field($this->model, $options['name'])->widget(\kartik\file\FileInput::classname(), [
                'options' => ['accept' => 'image/*'],
                'pluginOptions' => [
                    'allowedFileExtensions' => ['jpg','png','jpeg'],
                    'showUpload' => false,
                ],
            ])->label(false);
        }
        echo sprintf('<div class="col-sm-6 form-group"><label>%s</label><div>%s</div></div>', $options['label'], $html);
    }

    public function fieldRenderList($options)
    {
        if (is_string($options['items'])) {
            @list($type, $name) = explode('::', $options['items']);
            $options['items'] = [];
            if ($type == 'dictionary' && ($type = array_search($name, Dictionary::$controllerIds))) {
                $options['items'] = Dictionary::findActive($type)->select('title')->indexBy('id')->column();
            }
            if ($this->bulkMode) {
                $options['items'] = ['!' => '! - сбросить значение'] + $options['items'];
            }
        }

        if (!is_array($options['items'])) $options['items'] = [];

        if (!isset($options['inputOptions']['prompt'])) $options['inputOptions']['prompt'] = 'Выбрать вариант';
        $items = ArrayHelper::remove($options, 'items', ['' => 'Нет вариантов']);
        $field = $this->form
                ->field($this->model, $options['name'], ['options' => @$options['fieldOptions'] ?: []])
                ->dropDownList($items, @$options['inputOptions'] ?: []);
        if (@$options['label'] !== null) $field = $field->label($options['label']);
        echo $field;
    }

    public function fieldRenderClearfix()
    {
        echo '<div class="clearfix"></div>';
    }

    public function fieldRenderListManager($options)
    {
        $managers = User::find()
                        ->andWhere(['status' => User::STATUS_ACTIVE])
                        ->andWhere(['>=', 'role', User::USER_MANAGER])
                        ->select('id,username,email')->indexBy('id')->asArray()->all();
        $items = array_map(function($el){
            return "{$el['username']} [{$el['email']}]";
        }, $managers);
        $options['items'] = $items;
        return $this->fieldRenderList($options);
    }

    public function fieldRenderDateInput($options)
    {
        $options['inputOptions']['type'] = 'date';
        $this->fieldRenderTextInput($options);
    }

    public function fieldRenderMultiList($options)
    {
        if (is_string($options['items'])) {
            @list($type, $name) = explode('::', $options['items']);
            $options['items'] = [];
            if ($type == 'dictionary' && ($type = array_search($name, Dictionary::$controllerIds))) {
                $options['items'] = Dictionary::findActive($type)->select('title')->indexBy('id')->column();
            }
            if ($this->bulkMode) {
                $options['items'] = ['!' => '! - сбросить значение'] + $options['items'];
            }
        }

        if (!is_array($options['items'])) $options['items'] = [];

        if (!isset($options['inputOptions']['prompt'])) $options['inputOptions']['prompt'] = 'Выбрать вариант';
        $items = ArrayHelper::remove($options, 'items', ['' => 'Нет вариантов']);
        asort($items);

        $field = $this->form->field($this->model, $options['name'], ['options' => @$options['fieldOptions'] ?: []])
                    ->widget(\kartik\select2\Select2::classname(), [
                        'data' => $items,
                        'language' => 'ru',
                        'options' => array_merge(['multiple' => true, 'placeholder' => 'Select a state ...'], @$options['inputOptions'] ?: []),
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);

        if (@$options['label'] !== null) $field = $field->label($options['label']);
        echo $field;
    }

    public function registerJs()
    {
        $this->view->registerJs(<<<JS
    $('form [data-slave-to]').each(function(i, el){
        var source = $($(el).data('slave-to'), 'form').eq(0);
        if (source.length == 1) $(source).change(function(){ $(el).val($(this).val()); });
    });
JS
);
    }
}