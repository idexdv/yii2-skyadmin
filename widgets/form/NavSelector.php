<?php

namespace admin\widgets\form;

use Yii;
use yii\helpers\Url;
use yii\helpers\Html;
use common\models\Page;

/**
* NavSelector Form widget
*/
class NavSelector extends \yii\base\Widget
{
	public $form;
	public $model;
	public $targetId;

	public function run()
	{
		if (isset(Page::$controllerLinkable))
			$pageTypes = array_intersect_key(Page::$controllerTitles, array_flip(Page::$controllerLinkable));
		else
			$pageTypes = Page::$controllerTitles;

		echo '

		<div class="col-sm-12 form-group">
			<div class="panel panel-default">
				<div class="panel-heading">
					<!-- <a id="restoreUrl" class="pull-right" style="cursor: pointer;" data-url="' . urlencode($this->model['url']) . '">Вернуть URL</a> -->
					Выбрать объект для перехода
				</div>
			  	<div class="panel-body">
					<div class="row">
		';

		foreach ($pageTypes as $type => $label) {
			echo Html::tag('div',
				"<label>{$label}</label>" .
				\kartik\typeahead\Typeahead::widget([
				    'name' => 'country',
				    'options' => ['placeholder' => 'Filter as you type ...'],
				    'pluginOptions' => ['highlight'=>true],
				    'dataset' => [
				        [
				            'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('value')",
				            'display' => 'value',
				            'limit' => 10,
				            // 'prefetch' => $baseUrl . '/samples/countries.json',
				            'remote' => [
				                'url' => Url::to(['list-feed', 'type' => $type]) . '&q=%QUERY',
				                'wildcard' => '%QUERY'
				            ]
				        ]
				    ],
					'pluginEvents' => [
					    "typeahead:select" => "function(e, suggestion) {
					    	$('#navigator-url').val(suggestion.url);
					    	if ($('#navigator-title').val().length == 0) $('#navigator-title').val(suggestion.value);
					    	$('#{$this->targetId}').val(suggestion.target);
					   	}",
					],
				]),
				['class' => 'col-md-12 col-lg-6']
			);
		}

		echo '
					</div>
				</div>
			</div>
		</div>
		';

		$this->registerJs();
	}

	private function registerJs()
	{
		$this->view->registerJs("");
	}
}
