<?php

namespace admin\widgets\form;

use Yii;
use common\models\Image;
use kartik\file\FileInput;
use yii\helpers\Url;
use yii\helpers\Html;

/**
* Image control widget
*/
class ImageControl extends \yii\base\Widget
{
    public $model;
    private $_images;

    public function run()
    {
        if (Image::model2type($this->model) === false) return 'asd';

        if ($this->_images = $this->model->images) {
            $galleryWidget = \yii\grid\GridView::begin([
                'dataProvider' => new \yii\data\ArrayDataProvider([
                    'allModels' => $this->_images,
                    'pagination' => false,
                ]),
                'layout' => '{items}',
                'columns' => [
                    [
                        'format' => 'raw',
                        'value' => function($model) { return Html::a(Html::img($model->thumb, ['class' => 'img-thumbnail']), $model->frontImage, ['data-lightbox' => 'images', 'target' => '_blank']); },
                    ],
                    [
                        'format' => 'raw',
                        'value' => function($model) {
                            return
                                Html::a($model->filename, [$model->filename], ['target' => '_blank']) . '<br>' .
                                ($model->title ?: '- нет описания -');
                        },
                    ],
                    [
                        'format' => 'raw',
                        'value' => function($model) { return Html::a('Удалить', ['/admin/utils/img-delete', 'id' => $model->id], ['class' => 'text-danger', 'data-method' => 'post', 'data-confirm' => 'Удалить?']); },
                    ],
                ],
                'options' => ['class' => 'image-control-grid'],
            ]);
            $galleryWidget->end();

            if(count($this->_images) > 1) {
                \plugins\assets\SortableAsset::register($this->view);
                $this->view->registerJs("
                sortable('#{$galleryWidget->id} tbody', {
                    forcePlaceholderSize: true,
                    placeholderClass: 'sortable-placeholder',
                    placeholder: '<tr><td colspan=\"10\" class=\"text-center\">Место для переноса</td></tr>', 
                });
                $('#{$galleryWidget->id} tbody').on('sortupdate', function(e) {
                    let sorted = sortable(this, 'serialize')[0].children.map(function(el){ return el.dataset.key; });
                    $.post('" . Url::to(['/admin/utils/img-sort']) . "', { sort: sorted });
                });");
            }
        }

        echo FileInput::widget([
            'name' => 'FileUpload[imageFile]',
            'options'=>[
                'multiple' => true,
                'accept' => 'image/*',
            ],
            'pluginOptions' => [
                'allowedFileExtensions' => ['jpg','png','jpeg'],
                'uploadUrl' => Url::to(['/admin/utils/img-upload']),
                'uploadExtraData' => [
                    'FileUpload[oid]' => $this->model->id,
                    'FileUpload[mcl]' => $this->model->formName(),
                ],
                'showClose' => false,
                'maxFileSize' => 2800,
                'maxFileCount' => 10,
            ],
        ]);
    }
}
