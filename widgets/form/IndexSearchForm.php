<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>


<? $form = ActiveForm::begin(['action' => [''], 'method' => 'get', 'class' => 'search-form']); ?>
<div class="row">
    <div class="col-sm-12">
        <?= $form->field($searchModel, 'search', [
            'template' =>
                '<div class="input-group">' .
                    '{input}' .
                    '<span class="input-group-btn">' .
                        Html::button('Сбросить', ['class' => 'btn btn-default reset-search', 'onclick' => "$('#search-input').val(''); this.form.submit();"]) .
                        Html::submitButton('Найти', ['class' => 'btn btn-primary']) .
                    '</span>' .
                '</div>'
            ])->textInput(['placeholder' => 'Поиск...', 'id' => 'search-input']) ?>
    </div>
</div>

<? if (Yii::$app->request->get('type')) echo Html::hiddenInput('type', Yii::$app->request->get('type')); ?>
<? if (Yii::$app->request->get('per-page')) echo  Html::hiddenInput('per-page', Yii::$app->request->get('per-page')); ?>

<? ActiveForm::end(); ?>
