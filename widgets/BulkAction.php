<?php

/*
 * This file is part of the admin package.
 *
 * (c) Dmitry Vikharev <idexdv@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
 
namespace admin\widgets;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Url;

class BulkAction extends Widget
{
    public $sourceId;
    public $layout = '{active-on}{active-off}{delete}{sort}';
    public $bulkAction = 'bulk-action';
    public $sortAction = 'sort-action';

    public $items = [
        'active-on' => ['label' => '<i class="fa fa-power-off text-success"></i>&nbsp;Включить',  'url' => 'active-on' ],
        'active-off'=> ['label' => '<i class="fa fa-power-off text-warning"></i>&nbsp;Выключить', 'url' => 'active-off'],
        'delete'    => ['label' => '<i class="fa fa-trash text-danger"></i>&nbsp;Удалить',        'url' => 'delete-it'],
        'sort'      => [
                            'label' => '<i class="fa fa-bars text-danger"></i>&nbspВключить сортировку',
                            'options' => ['data-sort-control' => true, 'class' => 'pull-right']
                        ],
    ];

    public function run()
    {
        if (empty($this->items)) {
            return;
        }

        preg_match_all('/{([a-zA-Z-]+)}/', $this->layout, $layout);
        $items = array_map(function($element) {
                    if (!($item = @$this->items[$element])) return null;
                    if ($url = \yii\helpers\ArrayHelper::remove($item, 'url')) { $item['options']['data-bulk-action'] = $url; }
                    $item['options']['class'] = trim(@$item['options']['class'] . ' btn btn-default');
                    $item['encodeLabel'] = false;
                    if ($element == 'sort') {
                        $item['options']['data-target'] = $this->sourceId;
                        $this->registerSortJS();
                    }
                    return $item;
                }, $layout[1]);

        echo Html::tag('div',
            sprintf('%s<br>%s<div class="clearfix"></div>',
                Html::label('Групповые действия'),
                \yii\bootstrap\ButtonGroup::widget(['buttons' => $items, 'options' => ['data-source' => $this->sourceId, 'class' => 'bulk-action-group', 'style' => 'display: block;']])
            ));

        $actionUrl = explode('/', \Yii::$app->request->pathInfo, 3);
        $actionUrl[2] = $this->bulkAction;
        $url = '/' . implode('/', $actionUrl);

        $this->view->registerJs("
            $('[data-bulk-action]', '.bulk-action-group').click(function(e){
                var sourceId = $(this).closest('.bulk-action-group').data('source');
                    keys = $(sourceId).yiiGridView('getSelectedRows');
                if (keys.length > 0) {
                    if (confirm('Применить действие?')) {
                        $.post('{$url}', {action: $(this).data('bulk-action'), keys: keys})
                        .done(function(data) { if (data.result == true) location.reload(true); });
                    }
                } else {
                    $('[name=\"selection[]\"]', $(sourceId)).animate({ opacity: .3 }).animate({ opacity: 1 });
                }
            });
        ", \yii\web\View::POS_READY, 'bulk-action-js');
    }

    public static function actionButtons()
    {
        echo Html::a('<i class="fa fa-download"></i>&nbsp;Выгрузить', Url::current(['download' => true]),
            ['class' => 'btn btn-default pull-right', 'style' => 'margin-left: 5px; min-width: 105px;']);

        echo Html::a('<i class="fa fa-upload"></i>&nbsp;Загрузить', null,
            ['class' => 'btn btn-default pull-right', 'style' => 'margin-left: 5px; min-width: 105px;', 'data' => ['toggle' => 'modal', 'target' => '#upload-modal']]);

        \yii\bootstrap\Modal::begin(['id' => 'upload-modal', 'header' => '<h3>Загрузить файл</h3>']);
        $form = \yii\widgets\ActiveForm::begin(['action' => ['upload-data', 'cat_id' => @$_GET['ProductSearch']['category_id']], 'options' => ['enctype' => 'multipart/form-data']]);
        echo $form->field($model = new \admin\models\UploadForm, 'dataFile')->fileInput()->label(false);
        echo Html::submitButton('Загрузить', ['class' => 'btn btn-primary']);
        \yii\widgets\ActiveForm::end();
        \yii\bootstrap\Modal::end();
    }

    private function registerSortJS()
    {
        \yii\jui\JuiAsset::register($this->view);

        $actionUrl = explode('/', \Yii::$app->request->pathInfo, 3);
        $actionUrl[2] = $this->sortAction;
        $url = '/' . implode('/', $actionUrl);

        $this->view->registerJs(
<<<JS
        $('[data-sort-control]').click(function(e){
            var target = $(this).data('target');
            $(target + ' tbody').sortable();
            $(target + ' tbody').on('sortstop', function(event, ui){
                var rows = $.param($(target + ' tbody tr').map(function() {
                    return { name: $(this).data('key'), value: $(this).data('key') };
                }));
                $.post('{$url}', {data: rows}); 
            });
            $(this).fadeOut();
        });
JS
        );
        

    }
}
