<?php

namespace admin\widgets;

use yii\helpers\Html;

class ParentControlColumn extends \yii\grid\DataColumn
{
    public $modelName;
    public $attribute;

    protected function renderDataCellContent($model, $key, $index)
    {
        $linkIndex = \yii\helpers\Url::toRoute(['index', 'type' => $model::$controllerIds[$model->type], $this->modelName => ['parent_id' => $key]]);
        $linkCreate = \yii\helpers\Url::toRoute(['create', 'type' => $model::$controllerIds[$model->type], 'parent_id' => $key]);

        $childrenCount = $model::find()->where(['parent_id' => $model->id])->count();
        $html = $childrenCount > 0 ?
                    Html::a('<i class="fa fa-files-o"></i> (' . $childrenCount . ')', $linkIndex, ['data-pjax' => '0', 'class' => 'btn btn-default btn-sm btn-fw'])
                  : Html::a('Создать', $linkCreate, ['data-pjax' => '0', 'class' => 'btn btn-sm btn-fw btn-title-link']);
        return $html;
    }
}
