<?php

/*
 * This file is part of the admin package.
 *
 * (c) Dmitry Vikharev <idexdv@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
 
namespace admin\widgets;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\select2\Select2;

class TagHelper extends Widget
{
	public static function formGroup($form, $model)
	{
		$_html = '';

		foreach ($model->tagList as $tag) {
			$_html .=  $form->field($model, "tag[{$tag->id}][]")
							->widget(Select2::classname(), [
								'data' => $tag->variants,
								'language' => 'ru',
								'showToggleAll' => true,
								'maintainOrder' => true,
								'options' => [
									'multiple' => true,
								],
								'pluginOptions' => [
									'tags' => true,
									'maximumInputLength' => 5,
									'allowClear' => true
								],
							])->label($tag->title);
		}

		return $_html;
	}
}
