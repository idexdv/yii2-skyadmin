<?php

namespace admin\widgets;

use yii\helpers\Html;
use common\models\Image;

class PublicationColumn extends \yii\grid\DataColumn
{
    public $attribute = 'title';

    protected function renderDataCellContent($model, $key, $index)
    {
        $link = \yii\helpers\Url::toRoute(['update', 'id' => $key]);
        $short= \yii\helpers\StringHelper::truncate($model['content_short'], 100);
        $tags = $model->specialization;

        $html = <<<END
        <a href="{$link}">{$model['title']}</a>
        <p>{$short}</p>
        {$tags}
END;
		return $html;
    }
}
