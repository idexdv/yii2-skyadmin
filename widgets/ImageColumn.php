<?php

/*
 * This file is part of the admin package.
 *
 * (c) Dmitry Vikharev <idexdv@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace admin\widgets;

use yii\helpers\Html;
use common\models\Image;

class ImageColumn extends \yii\grid\Column
{
    public $header = '<span class="fa fa-image"></span>';
	public $headerOptions = ['class' => 'image-column'];
	public $contentOptions = ['class' => 'image-column'];
    public $imageClass;
    public $galleryImage = false;
    public $actionId = 'update';

    /**
     * @inheritdoc
     */
    protected function renderDataCellContent($model, $key, $index)
    {
        if ($this->galleryImage) {
            if (!method_exists($model, 'getImage()') && !($_img = @$model->image)) $_img = @$model->images[0];
            if (@$_img) $imgUrl = $_img->thumbnail; else $imgUrl = Image::$defaultImageUrl;
        }
        else
            $imgUrl = $model->thumbnail;

        return
            Html::a(Html::tag('div', '', ['style' => 'background-image: url(' . $imgUrl . ')', 'class' => 'image-column-img back-image']),
                [$this->actionId, 'id' => $key],
                ['data-pjax' => '0', 'title' => 'Войти']);
    }
}
