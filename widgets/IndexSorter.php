<?php

/*
 * This file is part of the admin package.
 *
 * (c) Dmitry Vikharev <idexdv@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
 
namespace admin\widgets;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Url;

class IndexSorter extends Widget
{
	public $indexId;

	public function run()
	{
		if (empty($this->indexId)) return;

		\yii\jui\JuiAsset::register($this->view);

		$_link = \yii\helpers\Url::to(['changeorder']);

		$this->view->registerJs("
			$('#{$this->indexId} tbody').sortable();
			$('#{$this->indexId} tbody').on('sortstop', function(event, ui){
			    var rows = $.param($('#{$this->indexId} tbody tr').map(function() {
			        return { name: $(this).data('key'), value: $(this).data('key') };
			    }));
			    $.post('{$_link}', { data: rows }); 
			});
		");
	}
}
